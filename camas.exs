#!/usr/bin/env elixir

# we can use IO.inspect() to dump a map (sort of like Object#inspect in Ruby
# looks like; precisely what I want right now).

IO.puts("Hello, camas.")
h = %{hello: "camas"}
IO.inspect(h)

# TODO(zmd): how do we make an http request and parse its response from JSON
#   into appropriate Elixir data structures?

