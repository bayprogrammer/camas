# Camas

Terminal pnut.io client for Linux/BSD/*nix power users.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `camas` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:camas, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/camas](https://hexdocs.pm/camas).

## Usage

Launch camas from the commandline like so:

    $ camas

## Development

TBD

## Contributing

Bug reports and pull requests are welcome on GitLab at
[https://gitlab.com/bayprogrammer/camas].

1. Fork this repo
1. Create your feature branch: git checkout -b feature-name
1. Commit your changes: git commit -am 'Add some feature'
1. Push to the branch: git push origin my-new-feature
1. Submit a pull request

## License

The gem is available as open source under the terms of the [MIT
License](LICENSE.txt).

## Code of Conduct

Everyone interacting in the Camas codebases, issue trackers, chat rooms and
mailing lists is expected to follow the [Code of Conduct](CODE_OF_CONDUCT.md).
