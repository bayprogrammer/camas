defmodule Camas do
  @moduledoc """
  Documentation for Camas.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Camas.hello()
      :world

  """
  def hello do
    :world
  end
end
